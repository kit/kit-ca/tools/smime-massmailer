package smime_massmailer

import (
	"bytes"
	"crypto/x509"
	"fmt"
	"log"
	"os/exec"
	"strings"
)

var OpenSSLPath string

func init() {
	var err error
	OpenSSLPath, err = exec.LookPath("openssl")
	if err != nil {
		log.Fatal(err)
	}
}

func OpenSSLGetDN(cert x509.Certificate) (string, error) {
	cmd := exec.Command(OpenSSLPath,
		"x509",
		"-inform",
		"DER",
		"-noout",
		"-subject",
		"-nameopt",
		"oneline,esc_ctrl,-esc_msb,-use_quote,utf8,sep_comma_plus_space,-space_eq,sname,-esc_2253")

	out := &bytes.Buffer{}
	errs := &bytes.Buffer{}
	cmd.Stdout, cmd.Stderr = out, errs

	// write cert to stdin
	stdin, err := cmd.StdinPipe()
	if err != nil {
		return "", err
	}
	go func() {
		defer stdin.Close()
		_, err = stdin.Write(cert.Raw)
		if err != nil {
			log.Fatal(err)
		}
	}()

	if err := cmd.Run(); err != nil {
		if len(errs.Bytes()) > 0 {
			return "", fmt.Errorf("error running %s (%s):\n %v", cmd.Args, err, errs.String())
		}
		return "", err
	}

	subject, _ := strings.CutPrefix(out.String(), "subject=")
	return strings.TrimSpace(subject), nil
}

func Openssl(stdin []byte, args ...string) ([]byte, error) {
	cmd := exec.Command(OpenSSLPath, args...)

	in := bytes.NewReader(stdin)
	out := &bytes.Buffer{}
	errs := &bytes.Buffer{}

	cmd.Stdin, cmd.Stdout, cmd.Stderr = in, out, errs

	if err := cmd.Run(); err != nil {
		if len(errs.Bytes()) > 0 {
			return nil, fmt.Errorf("error running %s (%s):\n %v", cmd.Args, err, errs.String())
		}
		return nil, err
	}

	return out.Bytes(), nil
}
