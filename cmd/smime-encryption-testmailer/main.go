package main

import (
	"bytes"
	"crypto/x509"
	_ "embed"
	"encoding/pem"
	"flag"
	"fmt"
	"log"
	"mime/multipart"
	"net/textproto"
	"os"
	"strings"
	"text/template"
	"time"

	"git.scc.kit.edu/heiko.reese/smime-massmailer/v2"
	"gopkg.in/gomail.v2"
)

//go:embed text.template
var emailBodyTemplateText string

//go:embed html.template
var emailBodyTemplateHTML string

var (
	subjectTemplateText = `🔐 Verschlüsselt für / Encrypted for {{.Serial}} {{- if gt (len .CommonName) 0 }} (CN: {{.CommonName}}){{- end}}; {{.ValidFrom}} – {{.ValidUntil}}`
	subjectTemplate     = template.Must(template.New("subject").Parse(subjectTemplateText))
)

type Config struct {
	recipient, sender string
	dump, send        bool
	mailhost          string
	mailport          int
}

type TemplateData struct {
	Cert    *x509.Certificate
	Subject string
}

var (
	appConfig        Config
	TextBodyTemplate *template.Template
	HTMLBodyTemplate *template.Template
)

func init() {
	var err error
	//flag.StringVar(&appConfig.bodyFile, "body", "body.template", "File containing email body template (optional)")
	//flag.StringVar(&appConfig.htmlFile, "html", "html.template", "File containing email html body template (optional)")
	flag.StringVar(&appConfig.sender, "sender", "KIT-CA <ca@kit.edu>", "Specify sender")
	flag.StringVar(&appConfig.recipient, "recipient", "", "Alternate recipient (optional, will use email from certificate if not specified)")
	flag.BoolVar(&appConfig.dump, "dump", false, "Write emails to files (default: false)")
	flag.BoolVar(&appConfig.send, "send", true, "Send emails (default: true)")
	flag.StringVar(&appConfig.mailhost, "mailhost", "smarthost.kit.edu", "Mailserver hostname")
	flag.IntVar(&appConfig.mailport, "port", 25, "Mailserver port")
	flag.Parse()

	// use AES-128 instead of 3DES (currently broken!)
	//pkcs7.ContentEncryptionAlgorithm = pkcs7.EncryptionAlgorithmAES128GCM

	// prepare text template
	RawTextBodyTemplate := []byte(emailBodyTemplateText)
	TextBodyTemplate = template.New("textBody")
	TextBodyTemplate = TextBodyTemplate.Funcs(template.FuncMap{"StringsJoin": strings.Join})
	TextBodyTemplate, err = TextBodyTemplate.Parse(string(RawTextBodyTemplate[:]))
	if err != nil {
		log.Fatalf("Error parsing body template: %s", err)
	}

	// prepare html template
	RawHTMLBodyTemplate := []byte(emailBodyTemplateHTML)
	HTMLBodyTemplate = template.New("htmlBody")
	HTMLBodyTemplate = HTMLBodyTemplate.Funcs(template.FuncMap{"StringsJoin": strings.Join})
	HTMLBodyTemplate, err = HTMLBodyTemplate.Parse(string(RawHTMLBodyTemplate[:]))
	if err != nil {
		log.Fatalf("Error parsing html body template: %s", err)
	}

}

func main() {
	var (
		err        error
		mailserver *gomail.Dialer
		localhost  string
	)
	// configure mailhost if needed
	if appConfig.send {
		localhost, err = os.Hostname()
		if err != nil {
			localhost = "localhorst" // this “typo” is on purpose
		}
		mailserver = gomail.NewDialer(appConfig.mailhost, appConfig.mailport, "", "")
		mailserver.LocalName = localhost
	}
	// check if certificates were specified
	if flag.NArg() < 1 {
		log.Fatal("Please specify at least one certificate")
	}

	// read and parse all certificates
	certs := ReadCertificates(flag.Args()...)
	if len(certs) < 1 {
		log.Fatal("No valid certificates found")
	}

	// generate a single email per certificate
	for filename, currentCerts := range certs {
		for _, cert := range currentCerts {
			// new email message
			message := gomail.NewMessage()

			// prevent auto-replies
			message.SetHeader("Auto-Submitted", "yes")
			message.SetHeader("Precedence", "bulk")
			message.SetHeader("X-Auto-Response-Suppress", "All")

			// Set recipient(s)
			recipients := make([]string, 0)
			if strings.Contains(appConfig.recipient, "@") {
				recipients = append(recipients, appConfig.recipient)
			} else {
				if len(cert.EmailAddresses) < 1 {
					log.Fatalf("Certificate %s does not contain email adresses", cert.SerialNumber.String())
				}
				recipients = append(recipients, cert.EmailAddresses...)
			}
			for _, recipient := range recipients {
				message.SetAddressHeader("To", recipient, cert.Subject.CommonName)
			}
			// set sender
			message.SetHeader("From", appConfig.sender)
			message.SetHeader("Reply-To", appConfig.sender)

			// get DN
			certificateDN, err := smime_massmailer.OpenSSLGetDN(*cert)
			if err != nil {
				log.Printf("Error getting subject for certificate with openssl: %s", err)
				// use stdlib instead
				certificateDN = cert.Subject.String()
			}

			// set email subject
			var certificateSubject string
			certificateSubject, err = ExtractCertificateSubject(cert)
			if err != nil {
				log.Fatalf("Error extracting certificate subject: %s", err)
			}
			message.SetHeader("Subject", certificateSubject)

			// write text body, execute template
			innerPart, _ := BuildEmailBody(cert, certificateDN)

			// encrypt text part
			encryptedPart, err := smime_massmailer.Openssl(innerPart,
				"cms",
				"-encrypt",
				"-outform",
				"DER",
				"-md",
				"SHA256",
				"-aes128",
				filename)
			if err != nil {
				log.Fatal(err)
			}

			// add encrypted body
			message.SetBody(`application/pkcs7-mime; name="smime.p7m"; smime-type=enveloped-data`,
				string(encryptedPart[:]),
				gomail.SetPartEncoding(gomail.Base64))

			// write output
			if appConfig.dump {
				filename := fmt.Sprintf("%s - %s.eml", strings.Join(recipients, ","), cert.SerialNumber.String())
				f, err := os.OpenFile(filename, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)
				if err != nil {
					log.Fatal(err)
				}
				_, err = message.WriteTo(f)
				_ = f.Close()
				if err != nil {
					log.Fatal(err)
				} else {
					log.Printf("Wrote %s\n", filename)
				}
			}

			if appConfig.send {
				// send email
				err = mailserver.DialAndSend(message)
				if err != nil {
					log.Fatal(err)
				} else {
					log.Printf("Sent e-mail for %s to %s", cert.SerialNumber.String(), strings.Join(recipients, ", "))
				}

			}
		}
	}
}

func BuildEmailBody(cert *x509.Certificate, subject string) ([]byte, error) {
	contents := &bytes.Buffer{}
	multipartAlternative := multipart.NewWriter(contents)

	_, _ = fmt.Fprintf(contents, "Content-Type: multipart/alternative;\n boundary=\"%s\"\n\n", multipartAlternative.Boundary())

	// render text body
	var TextBuffer bytes.Buffer
	err := TextBodyTemplate.Execute(&TextBuffer, TemplateData{
		Cert:    cert,
		Subject: subject,
	})
	if err != nil {
		log.Fatalf("Error executing body template: %s", err)
	}

	// render html body
	var HTMLBuffer bytes.Buffer
	err = HTMLBodyTemplate.Execute(&HTMLBuffer, TemplateData{
		Cert:    cert,
		Subject: subject,
	})
	if err != nil {
		log.Fatalf("Error executing body template: %s", err)
	}

	childContents, _ := multipartAlternative.CreatePart(textproto.MIMEHeader{"Content-Type": {"text/plain; charset=UTF-8"}, "Content-Transfer-Encoding": {"8bit"}})
	_, _ = childContents.Write(TextBuffer.Bytes())
	childContents, _ = multipartAlternative.CreatePart(textproto.MIMEHeader{"Content-Type": {"text/html; charset=UTF-8"}, "Content-Transfer-Encoding": {"8bit"}})
	_, _ = childContents.Write(HTMLBuffer.Bytes())

	_ = multipartAlternative.Close()

	return contents.Bytes(), err
}

func ExtractCertificateSubject(cert *x509.Certificate) (string, error) {
	var subjectBuf bytes.Buffer
	err := subjectTemplate.Execute(&subjectBuf, struct {
		Serial     string
		CommonName string
		ValidFrom  string
		ValidUntil string
	}{
		Serial:     cert.SerialNumber.String(),
		CommonName: cert.Subject.CommonName,
		ValidFrom:  cert.NotBefore.In(time.Local).Format("2.1.2006"),
		ValidUntil: cert.NotAfter.In(time.Local).Format("2.1.2006"),
	})
	if err != nil {
		log.Fatalf("Error executing subject template: %s", err)
	}
	return subjectBuf.String(), err
}

// ReadCertificates reads alls x509 certificates from a list of input files. Errors are logged and skipped.
func ReadCertificates(filenames ...string) map[string][]*x509.Certificate {
	var (
		allCerts = make(map[string][]*x509.Certificate)
		block    *pem.Block
	)
	for _, filename := range filenames {
		// read input file
		content, err := os.ReadFile(filename)
		if err != nil {
			log.Printf("Error reading file %s: %s", filename, err)
			continue
		}
		// try decoding as DER
		certs, err := x509.ParseCertificates(content)
		if err == nil {
			// found certs: append and go to next file
			allCerts[filename] = append(allCerts[filename], certs...)
			continue
		}
		// try decoding as PEM
		for block, content = pem.Decode(content); block != nil; block, content = pem.Decode(content) {
			// process only certificates
			if block.Type == "CERTIFICATE" {
				certs, err := x509.ParseCertificates(block.Bytes)
				if err == nil {
					allCerts[filename] = append(allCerts[filename], certs...)
				}
			}
		}
	}
	return allCerts
}
